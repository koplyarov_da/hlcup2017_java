import java.util.*;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.data.rest.webmvc.*;
import org.springframework.http.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.annotation.*;


@RestController
@EnableAutoConfiguration
public class Service {
	private static final String EMPTY_OPTIONAL_S = "Java 7 has no Optional :(";
	private static final Long EMPTY_OPTIONAL_L = new Long(42);

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public class BadRequestException extends RuntimeException {
		public BadRequestException(String msg) {
			super("Bad request: " + msg);
		}
	}


	public static class User {
		public long id;
		public String email = EMPTY_OPTIONAL_S;
		public String first_name = EMPTY_OPTIONAL_S;
		public String last_name = EMPTY_OPTIONAL_S;
		public String gender = EMPTY_OPTIONAL_S;
		public Long birth_date = EMPTY_OPTIONAL_L;

		@JsonIgnore
		public AbstractSet<Long> visitIds = new HashSet<Long>();

		public void merge(User update) {
			if (isEmpty(update.email)) email = update.email;
			if (isEmpty(update.first_name)) first_name = update.first_name;
			if (isEmpty(update.last_name)) last_name = update.last_name;
			if (isEmpty(update.gender)) gender = update.gender;
			if (isEmpty(update.birth_date)) birth_date = update.birth_date;
		}
	}

	public static class Location {
		public long id;
		public String place = EMPTY_OPTIONAL_S;
		public String country = EMPTY_OPTIONAL_S;
		public String city = EMPTY_OPTIONAL_S;
		public Long distance = EMPTY_OPTIONAL_L;

		@JsonIgnore
		public AbstractSet<Long> visitIds = new HashSet<Long>();

		public void merge(Location update) {
			if (isEmpty(update.place)) place = update.place;
			if (isEmpty(update.country)) country = update.country;
			if (isEmpty(update.city)) city = update.city;
			if (isEmpty(update.distance)) distance = update.distance;
		}
	}

	public static class Visit {
		public long id;
		public Long location = EMPTY_OPTIONAL_L;
		public Long user = EMPTY_OPTIONAL_L;
		public Long visited_at = EMPTY_OPTIONAL_L;
		public Long mark = EMPTY_OPTIONAL_L;

		public void merge(Visit update) {
			if (isEmpty(update.location)) location = update.location;
			if (isEmpty(update.user)) user = update.user;
			if (isEmpty(update.visited_at)) visited_at = update.visited_at;
			if (isEmpty(update.mark)) mark = update.mark;
		}
	}


	private Map<Long, User> users = new HashMap<Long, User>();
	private Map<Long, Location> locations = new HashMap<Long, Location>();
	private Map<Long, Visit> visits = new HashMap<Long, Visit>();


    @RequestMapping(value="/users/{userId}",method=RequestMethod.GET)
    User getUser(@PathVariable String userId) {
        User user = users.get(parseId(userId));
		if (user == null) throw new ResourceNotFoundException();
		return user;
    }

    @RequestMapping(value="/users/new",method=RequestMethod.POST)
    synchronized String newUser(@RequestBody User user) {
		if (user.id == 0) throw new BadRequestException("user.id");
		if (isEmpty(user.email)) throw new BadRequestException("user.email");
		if (isEmpty(user.first_name)) throw new BadRequestException("user.first_name");
		if (isEmpty(user.last_name)) throw new BadRequestException("user.last_name");
		if (isEmpty(user.gender)) throw new BadRequestException("user.gender");
		if (users.containsKey(user.id)) throw new BadRequestException("user.id");

		users.put(user.id, user);
		return "{}";
    }

    @RequestMapping(value="/users/{userId}",method=RequestMethod.POST)
    synchronized String updateUser(@PathVariable String userId, @RequestBody User update) {
        User user = users.get(parseId(userId));
		if (user == null) throw new BadRequestException("userId");

		if (update.email == null) throw new BadRequestException("user.email");
		if (update.first_name == null) throw new BadRequestException("user.first_name");
		if (update.last_name == null) throw new BadRequestException("user.last_name");
		if (update.gender == null) throw new BadRequestException("user.gender");

		user.merge(update);
		return "{}";
    }


    @RequestMapping(value="/locations/{locationId}",method=RequestMethod.GET)
    Location getLocation(@PathVariable String locationId) {
        Location location = locations.get(parseId(locationId));
		if (location == null) throw new ResourceNotFoundException();
		return location;
    }

    @RequestMapping(value="/locations/new",method=RequestMethod.POST)
    synchronized String newLocation(@RequestBody Location location) {
		if (location.id == 0) throw new BadRequestException("location.id");
		if (isEmpty(location.place)) throw new BadRequestException("location.place");
		if (isEmpty(location.country)) throw new BadRequestException("location.country");
		if (isEmpty(location.city)) throw new BadRequestException("location.city");
		if (isEmpty(location.distance)) throw new BadRequestException("location.distance");
		if (locations.containsKey(location.id)) throw new BadRequestException("location.id");

		locations.put(location.id, location);
		return "{}";
    }

    @RequestMapping(value="/locations/{locationId}",method=RequestMethod.POST)
    synchronized String updateLocation(@PathVariable String locationId, @RequestBody Location update) {
        Location location = locations.get(parseId(locationId));
		if (location == null) throw new BadRequestException("locationId");
		if (update.place == null) throw new BadRequestException("location.place");
		if (update.country == null) throw new BadRequestException("location.country");
		if (update.city == null) throw new BadRequestException("location.city");
		if (update.distance == null) throw new BadRequestException("location.distance");

		location.merge(update);
		return "{}";
    }


    @RequestMapping(value="/visits/{visitId}",method=RequestMethod.GET)
    Visit getVisit(@PathVariable String visitId) {
        Visit visit = visits.get(parseId(visitId));
		if (visit == null) throw new ResourceNotFoundException();
		return visit;
    }

    @RequestMapping(value="/visits/new",method=RequestMethod.POST)
    synchronized String newVisit(@RequestBody Visit visit) {
		if (visit.id == 0) throw new BadRequestException("visit.id");
		if (isEmpty(visit.location)) throw new BadRequestException("visit.locatoin");
		if (isEmpty(visit.user)) throw new BadRequestException("visit.user");
		if (isEmpty(visit.visited_at)) throw new BadRequestException("visit.visited_at");
		if (isEmpty(visit.mark)) throw new BadRequestException("visit.mark");
		if (visits.containsKey(visit.id)) throw new BadRequestException("visit.id");

		User u = users.get(visit.user);
		if (u != null) u.visitIds.add(visit.id);

		Location l = locations.get(visit.location);
		if (l != null) l.visitIds.add(visit.id);

		visits.put(visit.id, visit);
		return "{}";
    }

    @RequestMapping(value="/visits/{visitId}",method=RequestMethod.POST)
    synchronized String updateVisit(@PathVariable String visitId, @RequestBody Visit update) {
        Visit visit = visits.get(parseId(visitId));
		if (visit == null) throw new BadRequestException("visitId");
		if (update.location == null) throw new BadRequestException("visit.locatoin");
		if (update.user == null) throw new BadRequestException("visit.user");
		if (update.visited_at == null) throw new BadRequestException("visit.visited_at");
		if (update.mark == null) throw new BadRequestException("visit.mark");

		{
			User u = users.get(visit.user);
			if (u != null) u.visitIds.remove(visit.id);

			Location l = locations.get(visit.location);
			if (l != null) l.visitIds.remove(visit.id);
		}

		visit.merge(update);

		{
			User u = users.get(visit.user);
			if (u != null) u.visitIds.add(visit.id);

			Location l = locations.get(visit.location);
			if (l != null) l.visitIds.add(visit.id);
		}

		return "{}";
    }

	
	public static class VisitEntry {
		public long mark;
		public long visited_at;
		public String place;
	};

	public static class Visits {
		public List<VisitEntry> visits = new ArrayList<VisitEntry>();
	};

    @RequestMapping(value="/users/{userId}/visits",method=RequestMethod.GET)
    Visits getUserVisits(
		@PathVariable String userId, 
		@RequestParam(defaultValue="-10000000000") long fromDate, 
		@RequestParam(defaultValue="10000000000") long toDate, 
		@RequestParam(defaultValue="") String country, 
		@RequestParam(defaultValue="10000000000") long toDistance) 
	{
        User user = users.get(parseId(userId));
		if (user == null) throw new ResourceNotFoundException();

		Visits visitEntries = new Visits();
		for (Long visitId : user.visitIds) {
			final Visit v = visits.get(visitId);
			if (v == null) continue;
			final Location l = locations.get(v.location);
			if (l == null) continue;

			if (v.visited_at <= fromDate ||
				v.visited_at >= toDate ||
				(country != null && !country.isEmpty() && !country.equals(l.country)) ||
				l.distance >= toDistance)
			{ continue; }

			visitEntries.visits.add(new VisitEntry() {{ mark=v.mark; visited_at=v.visited_at; place=l.place; }});
		}
		return visitEntries;
    }


    @RequestMapping(value="/locations/{locationId}/avg",method=RequestMethod.GET)
    String getLocationAvg(
		@PathVariable String locationId,
		@RequestParam(defaultValue="-10000000000") long fromDate, 
		@RequestParam(defaultValue="10000000000") long toDate, 
		@RequestParam(defaultValue="-10000000000") long fromAge, 
		@RequestParam(defaultValue="10000000000") long toAge, 
		@RequestParam(defaultValue="") String gender)
	{
        Location location = locations.get(parseId(locationId));
		if (location == null) throw new ResourceNotFoundException();

		double sum = 0;
		double count = 0;

		for (Long visitId : location.visitIds) {
			final Visit v = visits.get(visitId);
			if (v == null) continue;
			final User u = users.get(v.user);
			if (u == null) continue;

			if (v.visited_at <= fromDate ||
				v.visited_at >= toDate ||
				(gender != null && !gender.isEmpty() && !gender.equals(u.gender)))
			{ continue; }

			sum += v.mark;
			++count;
		}

		return String.format(java.util.Locale.US, "{\"avg\":%.5f}", count == 0 ? 0.0 : (sum / count));
	}


	public static boolean isEmpty(String s) {
		return s == null || s == EMPTY_OPTIONAL_S;
	}

	public static boolean isEmpty(Long l) {
		return l == null || l == EMPTY_OPTIONAL_L;
	}

	public static long parseId(String idStr) {
		try { return Long.parseLong(idStr); }
		catch(Exception e) { throw new ResourceNotFoundException(); }
	}

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Service.class, args);
    }
}
